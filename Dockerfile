FROM python:3.7.4-buster

LABEL maintainer="Lucille DELAPORTE <lucille.delaporte@telecomnancy.eu>"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --assume-yes apt-utils

RUN apt-get -y upgrade

RUN mkdir -p /var/local/app; \
    mkdir -p /var/log/lde-snake-basket

RUN python -m venv /var/local/app/.venv

# Upgrade Python tool versions in virtual environment
RUN . /var/local/app/.venv/bin/activate; \
    pip install pip --upgrade; \
    pip install setuptools --upgrade

# Copy application files to /var/local/app
COPY app /var/local/app/
RUN chmod +x /var/local/app/*.sh

# Install dependencies into the application virtual environment
RUN . /var/local/app/.venv/bin/activate; \
    pip install -r /var/local/app/lde-snake-basket/requirements.txt

# Run Python application
CMD /var/local/app/run.sh
